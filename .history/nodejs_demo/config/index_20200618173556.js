const path = require('path')

const config = {
  WPS: {
    DOMAIN: "https://wwo.wps.cn",
    APP_ID: "1b111c73d66f41b147fabc93ef9a7bcb",
    APP_SECRET: "957e567d377e4ac56d75122ca70975b6",
    DOWNLOAD_URL: 'https://res-cn.makeblock.com/content/org/'
  },
  YOUR_SECRET_STRING: 'YOUR_SECRET_STRING',
  PORT: '19999',
  FILE_DIR: '/',
  VIEW_DIR: path.join(__dirname, '../../app')
}

module.exports =  config
